# Automation priority: null
# Test case importance: Low
# language: en
Feature: Create a new booking

	Scenario: Create a new booking
		Given Booking API is active
		When I POST a create booking
		Then I see response has 200 status code
		And I verify booking request response as per booking model