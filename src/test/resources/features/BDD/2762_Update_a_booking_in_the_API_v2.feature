# Automation priority: null
# Test case importance: Low
# language: en
Feature: Update a booking in the API v2

	Scenario Outline: Update a booking in the API v2
		Given Booking API is active
		When I UPDATE a booking |<firstname>|<lastnamne>|<totalprice>|<depositpaid>|<additionalneeds>|
		Then I see response has <code> status code
		And I verify booking request response as per booking model

		@Example_1
		Examples:
		| additionalneeds | code | depositpaid | firstname | lastnamne | totalprice |
		| "Lunch" | 200 | "True" | "Lease" | "Plan" | 100 |

		@Example_2
		Examples:
		| additionalneeds | code | depositpaid | firstname | lastnamne | totalprice |
		| "Lunch" | 401 | "false" | "John" | "Doe" | 200 |